#ifndef BOIDMANAGER_H
#define BOIDMANAGER_H

#include <list>

class Boid;

class BoidManager
{
public:
  static std::list<Boid*> boids;
  static std::list<Boid*> boidsToRemove;

  static void registerBoid(Boid* boid);
  static void deregisterBoid(Boid* boid);
  static void cleanup();
};

#endif

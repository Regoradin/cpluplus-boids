#ifndef BOID_H
#define BOID_H

#include "Ogre.h"

//not sure if this is right or will cause problems...
#include "boidManager.h"
#include "nodeManager.h"
#include "foodManager.h"

class Boid
{
 public:
  Boid(Ogre::SceneManager* sceneManager, Ogre::SceneNode* node, Ogre::Vector3 position = Ogre::Vector3(0, 0, 0), int foodType = 0);
  void speak();
  void doLoop();
  void kill();
    
  Ogre::Vector3 velocity;
  Ogre::Vector3 acceleration;
  Ogre::SceneNode* node;
  
private:
  Ogre::Entity* entity;
  Ogre::SceneManager* scene;

  float separationStrength;
  float alignmentStrength;
  float cohesionStrength;

  float foodSeekingStrength;
  float foodSeekingDistanceSquared;

  float centerSeekingStrength;
  float centerSeekingDistanceSquared;

  float foodEatingDistanceSquared;
  float hunger;
  float maxHunger;
  float hungerIncreaseRate;
  float hungerRestoredPerFood;

  float speed;

  int foodType;
};
#endif

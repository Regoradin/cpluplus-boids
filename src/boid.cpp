#include "boid.h"
#include "Ogre.h"
#include <iostream>

using Ogre::Vector3;

Boid::Boid(Ogre::SceneManager* sceneManager, Ogre::SceneNode* node, Ogre::Vector3 position, int foodType)
{
  scene = sceneManager;
  BoidManager::registerBoid(this);
  //NodeManager::registerNode(this);
  // entity = sceneManager->createEntity("boid");
  // node = parentNode->createChildSceneNode();
  // node -> attachObject(entity);

  this->node = node;
  
  node->setPosition(position);

  velocity =  Vector3(0, 0.5f, 0.5f);
  acceleration = Vector3(0, -0.02, 0);

  separationStrength = 0.3f;
  alignmentStrength = 0.25f;
  cohesionStrength = 0.2f;

  foodSeekingStrength = 1.0f;
  foodSeekingDistanceSquared = 14.0f;
  foodSeekingDistanceSquared *= foodSeekingDistanceSquared;

  foodEatingDistanceSquared = 1.0f;
  foodEatingDistanceSquared *= foodEatingDistanceSquared;

  hunger = 0.0f;
  hungerIncreaseRate = 0.01f;
  hungerRestoredPerFood = 5.0f;
  maxHunger = 10.0f;
  //TODO:
  // find a way for hunger to be in more reasonable values, so they're less jumpy
  // but also don't completely die a the start.
  // Maybe just cap the speed bonus they get from being hungry?

  centerSeekingStrength = 0.1f;
  centerSeekingDistanceSquared = 17.5f;
  centerSeekingDistanceSquared *= centerSeekingDistanceSquared;

  speed = 0.3;

  this->foodType = foodType;
}
  
void Boid::speak()
{
  std::cout << "BOID!" << std::endl;
}

void Boid::doLoop()
{
  hunger += hungerIncreaseRate;
  if (hunger > maxHunger)
    {
      hunger = maxHunger;
      std::cout << "DEAD";
      BoidManager::deregisterBoid(this);
    }
  
  // do boid update thing
  Vector3 separationVector = Vector3(0,0,0);
  Vector3 alignmentVector = Vector3(0,0,0);
  Vector3 cohesionVector = Vector3(0,0,0);

  Vector3 foodVector = Vector3(0,0,0);

  Vector3 centerSeekingVector = node->getPosition() * -1;

  if (centerSeekingVector.squaredLength() < centerSeekingDistanceSquared){
    centerSeekingVector*= 0;
  }


  int totalOtherBoids = 0;
    
  for (Boid* otherBoid : BoidManager::boids)
    {
      if (otherBoid == this){
	continue;
      }

      Vector3 towardsOther = (otherBoid->node->getPosition() - node->getPosition());
      towardsOther.normalise();

      //check if within cone of vision
      if (otherBoid->velocity.angleBetween(towardsOther) > Ogre::Radian(Ogre::Degree(90))){
	continue;
      }

      float distance = node->getPosition().squaredDistance(otherBoid->node->getPosition());

      if (distance > 30){
	continue;
      }

      totalOtherBoids++;
      
      separationVector -= towardsOther / distance;
      alignmentVector += otherBoid->velocity.normalisedCopy() / distance;
      cohesionVector += towardsOther;
    }

  if (totalOtherBoids != 0){
    cohesionVector /= totalOtherBoids;
  }

  Food* nearestFood = FoodManager::findClosestFoodToPoint(node, foodType);
  if (nearestFood){
    float distanceToFood = node->getPosition().squaredDistance(nearestFood->node->getPosition());
    if (distanceToFood < foodEatingDistanceSquared)
      {
	nearestFood->eat();
	hunger -= hungerRestoredPerFood;

	//Birth new boids
	for (int i = 0; i < 2; i++){
	  Ogre::Entity* entity = scene->createEntity("boid");
	  Ogre::SceneNode* newNode = scene->getRootSceneNode()->createChildSceneNode();
	  newNode->attachObject(entity);

	  Vector3 newPosition = node->getPosition() + (i * Vector3(0.1f, 0.1f, 0.1f));
	  new Boid(scene, newNode, newPosition, foodType);
	  new Food(scene, newNode, newPosition, 2);
	}
	
      }
    if (distanceToFood < foodSeekingDistanceSquared)
      {
	foodVector = nearestFood->node->getPosition() - node->getPosition();
	foodVector.normalise();
      }
  }

  separationVector *= separationStrength;
  alignmentVector *= alignmentStrength;
  cohesionVector *= cohesionStrength;

  foodVector *= foodSeekingStrength * hunger * 0.1f;

  centerSeekingVector *= centerSeekingStrength;

  Vector3 targetVelocity = separationVector + alignmentVector + cohesionVector + foodVector + centerSeekingVector;
  targetVelocity *= speed;

  velocity = Ogre::Math::lerp(velocity, targetVelocity, 0.2f);
    
  //do physics
  velocity += acceleration;
  node->translate(velocity);

  node->setDirection(velocity, Ogre::Node::TS_PARENT);
}

void Boid::kill(){
  node->removeAndDestroyAllChildren();
  scene->destroySceneNode(node);
}

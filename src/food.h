#ifndef FOOD_H
#define FOOD_H

#include "Ogre.h"
#include "foodManager.h"

class Food
{
 public:
  Food(Ogre::SceneManager* sceneManager, Ogre::SceneNode* node, Ogre::Vector3 position = Ogre::Vector3(0, 0, 0), int foodType = 0);

  void doLoop();

  void eat();
  Ogre::SceneNode* node;

  int foodType;
  
 private:
  Ogre::Entity* entity;
  Ogre::SceneManager* manager;
};

#endif

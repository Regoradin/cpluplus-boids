#include "boid_mesh.h"
#include "Ogre.h"

void Mesh::boidMesh(Ogre::SceneManager* sceneManager){
  // Ogre::String matName = "boid_mat";
  // Ogre::String groupName = "group";
  // Ogre::MaterialPtr mat = Ogre::MeshManager::getSingletonPtr()->create(matName, groupName);
  
  Ogre::ManualObject* man = sceneManager->createManualObject("boid_manualobject");
  man->begin("Examples/OgreLogo", Ogre::RenderOperation::OT_TRIANGLE_LIST);

  man->position(0, 0, -0.5);
  man->normal(0,0,1);
  man->textureCoord(0,0);
  
  man->position(0.5, 0, 0.5);
  man->normal(-1,0,0);
  man->textureCoord(0,0);
  
  man->position(0.5, 0, 0.5);
  man->normal(1,0,0);
  man->textureCoord(0,0);
    
  man->position(0, 0.2, 0.5);
  man->normal(0,1,0);
  man->textureCoord(0,0);
  
  man->position(0, -0.2, 0.5);
  man->normal(0,-1,0);
  man->textureCoord(0,0);

  man->triangle(0, 2, 3);
  man->triangle(0, 4, 2);
  man->triangle(0, 3, 1);
  man->triangle(0, 1, 4);
  man->quad (1, 3, 2, 4);

  man->end();
  man->convertToMesh("boid");

}

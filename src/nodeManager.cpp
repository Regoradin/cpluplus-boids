#include "nodeManager.h"

using Ogre::Node;
using std::list;

/*
CURRENTLY BROKEN

This needs to be refactored to have a proper Actor root object or something
which will hold all the components and the Node that a particular thing needs.

For now, using the hacky approach in BoidManager and FoodManager
*/

std::map<std::type_index, std::list<Node*>*> NodeManager::nodesByType;

void NodeManager::registerNode(Node* node){
  std::type_index nodeType = std::type_index(typeid(node));
  if (nodesByType.count(nodeType) == 0){
    nodesByType[nodeType] = new std::list<Node*>();
  }

  (*nodesByType[nodeType]).push_back(node);
}

list<Node*>* NodeManager::getAllNodesOfType(std::type_index type){
  if (nodesByType.count(type) == 0){
    return 0;
  }
  return nodesByType[type];
}
Node* NodeManager::getClosestNodeOfType(Ogre::Vector3 position, std::type_index type){
  if (nodesByType.count(type) == 0){
    return 0;
  }
  std::list<Node*>* nodes = nodesByType[type]; 
  
  float closestDistance = position.squaredDistance((*nodes).front()->getPosition());
  Node* closestNode = (*nodes).front();
  for(Node* node : *nodes){
    float distance = position.squaredDistance(node->getPosition());
    if (distance > closestDistance){
      closestDistance = distance;
      closestNode = node;
    }
  }
  
  return closestNode;
}



#ifndef BOID_MESH_H
#define BOID_MESH_H

#include "Ogre.h"

namespace Mesh
{
  void boidMesh(Ogre::SceneManager* sceneManager);
}

#endif

#ifndef FOODMANAGER_H
#define FOODMANAGER_H

#include "Ogre.h"
#include "food.h"

class Food;

class FoodManager{
 public:
  static void registerFood(Food* food);
  static void deregisterFood(Food* food);
  
  static std::list<Food*> getAllFoods();
  static Food* findClosestFoodToPoint(Ogre::SceneNode* node);
  static Food* findClosestFoodToPoint(Ogre::SceneNode* node, int foodType);
  
 private:
  static std::list<Food*> foods;
};
#endif

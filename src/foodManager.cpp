#include "foodManager.h"
#include <iostream>
#include <limits>

std::list<Food*> FoodManager::foods;

void FoodManager::registerFood(Food* food)
{
  foods.push_back(food);
}

void FoodManager::deregisterFood(Food* food)
{
  foods.remove(food);
}

std::list<Food*> FoodManager::getAllFoods()
{
  return foods;
}
Food* FoodManager::findClosestFoodToPoint(Ogre::SceneNode* node)
{
  return findClosestFoodToPoint(node, -1);
}
  
Food* FoodManager::findClosestFoodToPoint(Ogre::SceneNode* node, int foodType)
{
  Ogre::Vector3 point = node->getPosition();
  if (foods.size() == 0){
    return 0;
  }
  float closestDistance = std::numeric_limits<float>::max();
  Food* closestFood = nullptr;
  for(Food* food : foods){
    if (food->node != node)
      {
	float distance = point.squaredDistance(food->node->getPosition());
	if (distance < closestDistance && (foodType == -1 || foodType == food->foodType)){
	  closestDistance = distance;
	  closestFood = food;
	}
      }
  }
  return closestFood;

}

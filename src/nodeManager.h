#ifndef NODEMANAGER_H
#define NODEMANAGER_H

#include <list>
#include <map>
#include <typeindex>
#include <typeinfo>

#include "Ogre.h"
using Ogre::Node;

class NodeManager
{
 public:
  static void registerNode(Node* node);

  //probably make these template-y things?
  static std::list<Node*>* getAllNodesOfType( std::type_index type);
  static Node* getClosestNodeOfType(Ogre::Vector3 position, std::type_index type);

 private:
  static std::map<std::type_index, std::list<Node*>*> nodesByType;

};

#endif

#include "boidManager.h"
#include "boid.h"

std::list<Boid*> BoidManager::boids;
std::list<Boid*> BoidManager::boidsToRemove;
void BoidManager::registerBoid(Boid* boid)
{
  boids.push_back(boid);
}

void BoidManager::deregisterBoid(Boid *boid)
{
  boidsToRemove.push_back(boid);
}
void BoidManager::cleanup()
{
  for(Boid* boid : boidsToRemove){
    boids.remove(boid);
    boid->kill();
  }
  boidsToRemove.clear();
}

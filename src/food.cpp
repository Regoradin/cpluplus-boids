#include "food.h"
#include "Ogre.h"

#include <iostream>

Food::Food(Ogre::SceneManager* sceneManager, Ogre::SceneNode* node, Ogre::Vector3 position, int foodType)
{
  FoodManager::registerFood(this);

  // entity = sceneManager->createEntity("boid");
  // entity->setMaterialName("Template/Red", "Oliver");
  // node = parentNode->createChildSceneNode();
  // node -> attachObject(entity);

  this-> node = node;

  node->setPosition(position);

  manager = sceneManager;

  this->foodType = foodType;
}

void Food::eat()
{
  FoodManager::deregisterFood(this);
  node->removeAndDestroyAllChildren();
  manager->destroySceneNode(node);
}

void Food::doLoop()
{
  node->translate(Ogre::Vector3(0, -0.1, 0));

  if (node->getPosition().y < -20){
    this->eat();
  }
}

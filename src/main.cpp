#include "Ogre.h"
#include "OgreApplicationContext.h"
#include <iostream>
#include <chrono>
#include <thread>

#include "boid.h"
#include "boidManager.h"
#include "boid_mesh.h"
#include "food.h"
#include "foodManager.h"

using namespace Ogre;

class KeyHandler : public OgreBites::InputListener
{
  bool keyPressed(const OgreBites::KeyboardEvent& event) override
  {
    if (event.keysym.sym == OgreBites::SDLK_ESCAPE)
      {
	Ogre::Root::getSingleton().queueEndRendering();
      }
    return true;
  }
};

float randInRange(float center, float range){
  //return center + (rand() % range) - (range / 2);
  float result = (float)rand() / RAND_MAX * range + (center - range/2);
  return result;
}

void doGameLoop(Ogre::SceneManager* sceneManager)
{
  for (Boid* boid : BoidManager::boids)
    {
      boid->doLoop();
    }
  BoidManager::cleanup();
  for (Food* food : FoodManager::getAllFoods())
    {
      food->doLoop();
    }

  // Food Spawning
  if (randInRange(0.5, 1) < 0.1){
    Ogre::SceneNode* newNode = sceneManager->getRootSceneNode()->createChildSceneNode();
    Ogre::Entity* newEntity = sceneManager->createEntity("boid");
    newEntity->setMaterialName("Template/Red", "Oliver");
    newNode->attachObject(newEntity);
    
    new Food(sceneManager,
	     newNode,
  	     Vector3(randInRange(0, 40), 30, randInRange(0, 40)),
	     1);
  }
}

int main()
{
  OgreBites::ApplicationContext ctx("OgreTutorialApp");
  ctx.initApp();

  Ogre::Root* root = ctx.getRoot();
  Ogre::SceneManager*  sceneManager = root->createSceneManager();

  Ogre::ResourceGroupManager* resourceManager = Ogre::ResourceGroupManager::getSingletonPtr();
  resourceManager->addResourceLocation("/home/oliver/boids/boids/src/materials", "FileSystem", "Oliver");
  resourceManager->initialiseAllResourceGroups();

  Ogre::MaterialManager::getSingleton().load("foodMat.material", "Oliver");

  Ogre::RTShader::ShaderGenerator* shadergen = Ogre::RTShader::ShaderGenerator::getSingletonPtr();
  shadergen->addSceneManager(sceneManager);

  Ogre::Light* light = sceneManager->createLight("MainLight");
  Ogre::SceneNode* lightNode = sceneManager->getRootSceneNode()->createChildSceneNode();
  lightNode->setPosition(0, 10, 15);
  lightNode->attachObject(light);

  sceneManager->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));

  //Mesh Initialization
  Mesh::boidMesh(sceneManager);
  

  Ogre::SceneNode* camNode = sceneManager->getRootSceneNode()->createChildSceneNode();
  camNode->setPosition(0, 0, 50);
  camNode->lookAt(Ogre::Vector3(0, 0, -1), Ogre::Node::TS_PARENT);

  Ogre::Camera* cam = sceneManager->createCamera("myCam");
  cam->setNearClipDistance(5);
  cam->setAutoAspectRatio(true);
  camNode->attachObject(cam);

  ctx.getRenderWindow()->addViewport(cam);

  // Ogre::Entity* entity = sceneManager->createEntity("Sinbad.mesh");
  // Ogre::SceneNode* node = sceneManager->getRootSceneNode()->createChildSceneNode();
  // node -> attachObject(entity);

  // lightNode->getParent()->removeChild(lightNode);
  // node->addChild(lightNode);

  KeyHandler keyHandler;
  ctx.addInputListener(&keyHandler);

  //  ctx.getRoot()->startRendering();

  //Randomness Setup
  srand(0);


  //SETUP
  int boidCount = 100;
  for (int i = 0; i < boidCount; i++){
    Ogre::Entity* entity = sceneManager->createEntity("boid");
    Ogre::SceneNode* node = sceneManager->getRootSceneNode()->createChildSceneNode();
    node->attachObject(entity);


    Vector3 newPosition = Vector3(randInRange(0, 20), randInRange(0, 20), randInRange(0, 20));
    
    new Boid(sceneManager,
    	     node,
	     newPosition,
    	     1);

    new Food(sceneManager, node, newPosition, 2);

  }  
  
  while(true){
    root->renderOneFrame();
    doGameLoop(sceneManager);

    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  
  ctx.closeApp();
  
  return 0;
}

